import React, { Component } from 'react'; //no extension is ok
import ReactDOM from 'react-dom';

import JSON from './db.json';

// COMPONENTS
import Header from './components/header'
import NewsList from './components/news_list'

class App extends Component { //when return only have one enclosing tag
  state = {
    news    : JSON,
    filtered: []
  }

  getKeyword = event => {
    let keyword  = event.target.value;
    let filtered = this.state.news.filter((item) => {
      return item.title.indexOf(keyword) > -1;
    })
    this.setState({
      filtered
    });
  }

  render() {
    let newsResult = this.state.filtered.length ? this.state.filtered : this.state.news;
    return (
      <div>
        <Header keywords={this.getKeyword}/>
        <NewsList news={ newsResult }>
          <h3>The news are:</h3>
          <hr/>
        </NewsList>
      </div>
    )
  }
}

ReactDOM.render(<App/>, document.querySelector('#root'));