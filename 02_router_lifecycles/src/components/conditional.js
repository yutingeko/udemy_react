import React  from 'react'

const Conditional = () => {

  const returnVal = () => {
    return true
  }

  const showIt = () => {
    return (
      returnVal() ?
      <div>It's true</div> :
      <div>It's false</div>
    )
  }

  return (
    <div>
      { showIt() }
    </div>
  )
}

export default Conditional;