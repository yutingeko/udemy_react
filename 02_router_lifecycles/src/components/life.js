import React, { Component, PureComponent } from 'react'

class Life extends PureComponent {
  // 1 get default props

  // 2 set default state
  state = {
    title: 'Life cycles'
  }


  // // 3 before render
  // componentWillMount() {
  //   console.log('3 before render')
  // }

  // componentWillUpdate() {
  //   console.log('BEFORE UPDATE')
  // }

  // componentDidUpdate() {
  //   console.log('AFTERUPDATE')
  // }

  // // A interrupted behavior ,and it should return 'Boolean'
  // // true: go on next line; false: break;
  // shouldComponentUpdate(nextProps, nextState) {
  //   if(nextState.title === this.state.title) {
  //     return false
  //   }

  //   return true
  // }

  // componentWillReceiveProps() {
  //   console.log('BEFORE RECEIVE PROPS');
  // }

  // componentWillUnmount() {
  //   console.log('UNMOUNT')
  // }

  // 4 render jxs
  render() {
    console.log('RENDER')
    return (
      <div>
        <h3>{ this.state.title }</h3>
        <button onClick={()=> this.setState({
          title: 'something else'
        })}>CLICK TO CHANGE</button>
      </div>
    )
  }

  // // 5 after render
  // componentDidMount() {
  //   console.log('5 after render')
  // }
}

// const Life = () => {
//   return (
//     <div>Life</div>
//   )
// }

export default Life;