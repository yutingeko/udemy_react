import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Route, Link, NavLink, Switch, Redirect } from 'react-router-dom'

//react-router-dom : the introduction of differ route method
// BrowserRouter http://localhost:3000/posts （網址列直接帶切換的route name）
// HashRouter    http://localhost:3000/#/posts （網址列帶hash，之後才是路徑）
// MemoryRouter  http://localhost:3000/ (網址列不變但內容變換)

// COMPONENTS
import Home from './components/home'
import Posts from './components/posts'
import Profile from './components/profile'
import PostItem from './components/post_item'
import Life from './components/life'
import Conditional from './components/conditional'
import User from './components/user'

// myawesomeapp.com/posts
// myawesomeapp.com/profile/posts

const App = () => {
  return (
    <BrowserRouter>
      <div>
        <header>
          <NavLink to="/">Home</NavLink><br/>
          <NavLink
            to="/posts"
            activeStyle={{color: 'red'}}
            activeClassName="selected"
          >Posts</NavLink><br/>
          <NavLink to={{
            pathname: '/profile',
            // hash: '#francis',
            // search: '?profile=true'
          }}>profile</NavLink><br/>
          <NavLink to="/life">Life</NavLink><br/>
          <NavLink to="/conditional">Conditional</NavLink><br/>
          <NavLink to="/user">User</NavLink>
          <hr/>
        </header>
        <Switch>
          {/* <Redirect from="/profile" to="/" />> */}
          <Route path="/posts/:id/:username" component={PostItem}/>
          <Route path="/profile" component={Profile}/>
          <Route path="/posts" component={Posts}/>
          <Route path="/life" component={Life}/>
          <Route path="/conditional" component={Conditional}/>
          <Route path="/user" component={User}/>
          <Route path="/" exact component={Home}/>
          {/* <Route render={()=> `Oops! 404`}/> */}
          <Route component={Posts}/>
        </Switch>
      </div>
    </BrowserRouter>
  )
}

ReactDOM.render(
  <App/>,
  document.querySelector('#root')
)