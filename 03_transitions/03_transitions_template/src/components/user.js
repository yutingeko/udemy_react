import React, { Component } from 'react';
import UserTemplate from './user_template';

class User extends Component {

  state = {
    name: 'Ann',
    lastname: 'Jones',
    age: 25,
    hobbies: ['run', 'jump'],
    spanish: false,
    message(){console.log('hey')},
    car: {brand: 'Ford', modal: 'Focus'},
    mother: 'Martha'
  }

  render() {
    return (
      <div>
        <UserTemplate {...this.state}/>
      </div>
    );
  }
}

export default User;