import * as firebase from 'firebase'
import config from './config'

firebase.initializeApp(config)

const firebaseDB = firebase.database();
const googleAuth = new firebase.auth.GoogleAuthProvider();


export {
  firebase,
  firebaseDB,
  googleAuth
}

// firebaseDB.ref('users').push({
//   name: 'Steve',
//   lastname: 'Ball'
// })

// firebaseDB.ref('users').orderByChild('lastname').equalTo('Ball').once('value')
// .then(snapshot => console.log(snapshot.val()))

// firebaseDB.ref('users').orderByChild('age').limitToFirst(1).once('value')
// .then(snapshot => {
//   const users = [];

//   snapshot.forEach(childSnapshot => {
//     users.push({
//       id: childSnapshot.key,
//       ...childSnapshot.val()
//     })
//   })
//   console.log(users)
// })

// ref() reference data
// [empty] => all data
// [key] => value
// [parent key/children key] => descendants data

// Methods
// once('value') => get data (through promise)
// on('value')   => get data (through callback)
//   value => get all data
//   child_removed => when the data is removed, it will run callback
//   child_changed => when the data is changed, it will run callback
//   child_added => when the data is added, it will run callback
// set({})       => set data / insert data / update single data
// update({})    => update data
// remove()      => remove data

// GET DATA **********
// firebaseDB.ref().once('value')
// .then(snapshot => console.log(snapshot.val()))
// .catch(e => console.log(e))
// firebaseDB.ref().on('value', snapshot => {
//   console.log(snapshot.val())
// })
// firebaseDB.ref().on('child_added', snapshot => {
//   console.log(snapshot.key ,snapshot.val())
// })

// INIT DATA ***************
// firebaseDB.ref().set({
//   name: 'Francis',
//   lastname: 'Jone',
//   age: 20,
//   car: {
//     brand: 'Ford',
//     color: 'Black'
//   },
//   parents: ['Mario', 'Martha']
// })

// UPDATE DATA ******************
// firebaseDB.ref('lastname').set('Richardson')
// firebaseDB.ref('car/brand').set('Toyota')
// firebaseDB.ref().update({
//   name: 'Steve',
//   'car/color': 'Black'
// })
// .then(() => console.log('data updated'))
// .catch(e => console.log(e))

// INSERT DATA ****************
// firebaseDB.ref('skills').set({
//   skills:['Talking', 'Walking']
// })
// firebaseDB.ref('eyes').set('green')
// .then(() => console.log('data saved'))
// .catch((e) => console.log(e))

// REMOVE DATA ****************
// firebaseDB.ref('eyes').remove()  //method 1
// firebaseDB.ref('skills').set(null)  //method 2
// .then(() => console.log('data removed'))
// .catch(e => console.log(e))


// setTimeout(() => {
//   firebaseDB.ref('name').set('name 1')
// }, 3000)

// // TURN OFF the connection *****
// setTimeout(() => {
//   firebaseDB.ref().off()
// }, 4000)

// // The data can be store in the database,
// // but it can't affect webpage data
// setTimeout(() => {
//   firebaseDB.ref('name').set('name 2')
// }, 5000)